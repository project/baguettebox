<?php

/**
 * @file
 * Post-update functions for Baguettebox.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Adds selector setting to formatter.
 */
function baguettebox_post_update_add_selector_option(&$sandbox = []): void {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater->update($sandbox, 'entity_view_display', function (EntityViewDisplayInterface $view_display) use ($config_entity_updater): bool {
    $changed = FALSE;

    foreach ($view_display->getComponents() as $field => $component) {
      if (array_key_exists('type', $component)
        && ($component['type'] === 'baguettebox')
        && !array_key_exists('selector', $component['settings'])) {
        $component['settings']['selector'] = '.baguettebox';
        $view_display->setComponent($field, $component);
        $changed = TRUE;
      }
    }
    return $changed;
  });
}
