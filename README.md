# BaguetteBox

This module provides drupal integration with BaguetteBox library and
'Baguette Box' formatter for core image field type.

## Table of contents

 - Requirements
 - Installation
 - Configuration
 - Maintainers


## Requirements

This module doesn't depend on any other modules.
The only requirement is to download library
from <https://github.com/feimosi/baguetteBox.js>.


## Installation

=> Via Manual Mode:

 - Install this module as you would normally install a contributed
   Drupal module. Visit
   <https://www.drupal.org/docs/extending-drupal/installing-modules>
   for further information.
 - Download the BaguetteBox library from
   <https://github.com/feimosi/baguetteBox.js>.
   Use the exact same version as defined in baguettebox.libraries.yml file
 - Unzip the library and place files to libraries directory.
   The directory structure should be as follows:
   - /libraries/baguettebox.js/baguetteBox.min.css
   - /libraries/baguettebox.js/baguetteBox.min.js

=> Via Composer:

 - Add the following item to the "repositories" section of your composer.json:
 ```
   "repositories":
    {...},
    {
        "type": "package",
        "package": {
            "name": "feimosi/baguettebox.js",
            "version": "1.11.1",
            "type": "drupal-library",
            "dist": {
                "url": "https://github.com/feimosi/baguetteBox.js/releases/download/v1.11.1/baguetteBox.js.zip",
                "type": "zip"
            }
        }
    }
```

 - Download the BaguetteBox library from
   <https://github.com/feimosi/baguetteBox.js>.
   Use exact same version as defined in baguettebox.libraries.yml file

 - Unzip the library and place files to libraries directory.
   The directory structure should be as follows:
   - /libraries/baguettebox.js/baguetteBox.min.css
   - /libraries/baguettebox.js/baguetteBox.min.js


## Configuration

 -  To configure this module, you need to create a new content type,
    In order to do this, go to Structure -> Content types -> Add Content type
    and call it "Baguette.box" and click save.

    The next step is to go to Manage Fields and then click on "Add field".
    In the new field, select "image" and fill the label with a significative
    name such as "Baguette Box Image".

    The last step is to go to "Manage display"  and change,
    the format settings of the image style to "Baguette Box".

  - To make it work with Views, you should either set "Use field template"
    checkbox or manually add "baguettebox" class in View field style settings.


## Maintainers

Current maintainers:

 - Ivan - [Chi](https://www.drupal.org/u/chi)
 - Elber Rodrigues - [elber](https://www.drupal.org/u/elber)
