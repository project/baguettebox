<?php

/**
 * @file
 * Primary module hooks for Baguettebox module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme().
 */
function baguettebox_theme(): array {
  return [
    'baguettebox_formatter' => [
      'variables' => [
        'item' => NULL,
        'item_attributes' => NULL,
        'link_attributes' => NULL,
        'url' => NULL,
        'image_style' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook help().
 */
function baguettebox_help($route_name) {
  if ($route_name == 'help.page.baguettebox') {
    $output = '<h1>' . t('About') . '</h1>';
    $output .= '<p>' . t('This module provides drupal integration with BaguetteBox library and Baguette Box formatter for core image field type.') . '</p>';
    $output .= '<p>' . t('Install the baguettebox library e put in /libraries/baguettebox.js/baguetteBox.min.css: <a href="https://github.com/feimosi/baguetteBox.js">Baguettebox library</a>') . '</p>';
    $output .= '<h1>' . t('Configuration') . '</h1>';
    $output .= '<p>' . t('<ol><li> Create a content type </li>') . '</p>';
    $output .= '<p>' . t('<li> Add an image field type </li>') . '</p>';
    $output .= '<p>' . t('<li> Change the image format to "Baguette Box" </li> </ol>') . '</p>';
    $output .= '<p>' . t('To make it work with Views, you should either set "Use field template" checkbox or manually add "baguettebox" class in View field style settings.') . '</p>';
    return $output;
  }
}

/**
 * Prepares variables for baguettebox formatter templates.
 *
 * Default template: baguettebox-formatter.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An ImageItem object.
 *   - item_attributes: An optional associative array of html attributes to be
 *     placed in the img tag.
 *   - image_style: An optional image style.
 *   - url: An optional \Drupal\Core\Url object.
 */
function template_preprocess_baguettebox_formatter(array &$variables): void {
  $moduleHandler = \Drupal::moduleHandler();
  $moduleHandler->loadInclude('image', 'inc', 'image.field');
  template_preprocess_image_formatter($variables);
  $variables['link_attributes'] = new Attribute($variables['link_attributes']);
}
