<?php

declare(strict_types=1);

namespace Drupal\Tests\baguettebox\Functional;

use Drupal\Tests\image\Functional\ImageFieldTestBase;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Class to functional tests in baguettebox module.
 */
class BaguetteboxTest extends ImageFieldTestBase {
  use TestFileCreationTrait {
    getTestFiles as drupalGetTestFiles;
    compareFiles as drupalCompareFiles;
  }
  /**
   * Administrative user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritDoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'node',
    'user',
    'system',
    'image',
    'field',
    'field_ui',
    'baguettebox',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([], 'user', TRUE);
  }

  /**
   * This test simulates the creation of the BaguetteBox content.
   *
   * It also creates a field of type image, changes its format to
   * BaguetteBox and add an image in BaguetteBox content type.
   */
  public function testContentType(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalCreateContentType([
      'type' => 'baguettebox',
      'name' => 'baguettebox',
    ]);
    $this->drupalGet('admin/structure/types/manage/baguettebox/fields/add-field');
    // Add a field.
    $this->submitForm([
      'new_storage_type' => 'file_upload',
    ], 'Continue');
    $this->submitForm([
      'field_name' => 'image_baguettebox',
      'label' => 'Image Baguettebox',
      'group_field_options_wrapper' => 'image',
    ], 'Continue');
    $this->submitForm([], 'Update settings');
    $field_settings = [
      'settings[file_directory]' => 'test-upload',
    ];
    $this->submitForm($field_settings, 'Save settings');

    $this->drupalGet('/admin/structure/types/manage/baguettebox/display');
    // Change to Baguettebox format.
    $this->submitForm(["fields[field_image_baguettebox][type]" => "baguettebox"], 'Save');
    // Add baguettebox node.
    $this->drupalGet('node/add/baguettebox');
    // Add an image in image_baguettebox field.
    $image_files = $this->drupalGetTestFiles('image');
    $alt = $this->randomMachineName();
    $this->uploadNodeImage($image_files[0], 'field_image_baguettebox', 'baguettebox', $alt);
    $expected_path = 'public://test-upload';
    $this->assertFileExists($expected_path . '/' . $image_files[0]->filename);
    // Check if the content type has been created.
    $this->drupalGet('node/1');
  }

  /**
   * Test if the BaguetteBox module has been uninstalled correctly.
   */
  public function testUninstallBaguetteBox(): void {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/modules/uninstall');
    $this->submitForm(["uninstall[baguettebox]" => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
  }

}
